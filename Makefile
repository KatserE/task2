OUTPUT	:= "./output/"
SRC		:= "task2.swift"
NAME	:= "task2"

all:
	@test -d $(OUTPUT) || mkdir $(OUTPUT)
	@swiftc $(SRC) -o $(OUTPUT)/$(NAME)
	@echo Compilation is successful!

clean:
	@test -d $(OUTPUT) && rm -rf $(OUTPUT)
