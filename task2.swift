let monthsNames = [
    "January", "February", "March", "April", "May", "June", "July", "August",
    "September", "October", "November", "December"
]
let numOfDaysInMonths = [
    31, 28, 31, 30, 31, 30, 31, 31,
    30, 31, 30, 31
]

assert(monthsNames.count == numOfDaysInMonths.count)

// Only numbers of days
for numOfDays in numOfDaysInMonths {
    print(numOfDays)
}

print() // For separate outputs
// Months' names and number of days
for (name, numOfDays) in zip(monthsNames, numOfDaysInMonths) {
    print("\(name) -- \(numOfDays)")
}

print() // For separate outputs
let months: [(name: String, numOfDays: Int)] = Array(zip(monthsNames, numOfDaysInMonths))
// Months' names and number of days in tuples
for month in months {
    print("\(month.name) -- \(month.numOfDays)")
}

print() // For separate outputs
// Months' names and number of days in tuples in reversed order
for i in stride(from: months.count - 1, through: 0, by: -1) {
    print("\(months[i].name) -- \(months[i].numOfDays)")
}

// --------- Count date from user ---------
func getNumFromUser(label: String, range: ClosedRange<Int>) -> Int {
    while true {
        print(label, terminator: "")
        let input: String! = readLine()

        if let number = Int(input) {
            if range.contains(number) {
                return number
            }
        }

        print("Uncorrect number! Try again.")
    }
}

print() // For separate outputs
var monthIndex = getNumFromUser(
    label: "Enter a number from 1 to 12 to choose the month -- ",
    range: 1...12
) - 1
var date = getNumFromUser(
    label: "Enter a number from 1 to \(numOfDaysInMonths[monthIndex]) -- ",
    range: 1...numOfDaysInMonths[monthIndex]
)

let numDaysFromStart = numOfDaysInMonths[0..<monthIndex].reduce(0, +) + date - 1
print("From the beginning of the year to \(date) \(monthsNames[monthIndex]) \(numDaysFromStart) days.")
